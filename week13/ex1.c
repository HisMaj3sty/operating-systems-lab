#include <stdio.h>
#include <stdlib.h>

int C[1000][1000];
int R[1000][1000];
int E[1000];
int A[1000];

//This program will  read from stdin instead of hard-coded file
int main()
{
    int n; // number of processes
    int m; // number of resourses
    
    scanf("%d", &n);
    scanf("%d", &m);
    
    
    
    for (int i = 0;i <m;i++)
    {
        scanf("%d", &E[i]);
    }
    for (int i = 0;i <m;i++)
    {
        scanf("%d", &A[i]);
    }
    for (int i = 0;i <n;i++)
    {
        for (int j = 0;j <m;j++)
        {
            scanf("%d", &C[i][j]);
        }
    }
    
    for (int i = 0;i <n;i++)
    {
        for (int j = 0;j <m;j++)
        {
            scanf("%d", &R[i][j]);
        }
    }
    
    
    
    int flag1 = 1;
    while(flag1==1)
    {
        flag1 = 0;
        for (int i = 0;i < n;i++)
        {
            int flag = 0;
           
            for (int j = 0;j < m;j++)
            {
                if (R[i][j]>A[j] || R[i][j] == -1)
                {
                    flag = 1;
                    break;
                }

            }
            if (flag == 0)
            {
                for (int j = 0; j<m;j++)
                {
                    A[j]+=C[i][j];
                    R[i][j] = -1;
                }
                printf("\n");
                flag1 = 1;
            }
        }
    }
    int flag3 = 0;
    for (int i = 0;i <n;i++)
    {
        int flag2 = 0;
        for (int j = 0;j <m;j++)
        {
            if (R[i][j]!=-1)
            {
                flag2 = 1;
                break;
            }
        }
        if (flag2 == 1)
        {
            flag3 = 1;
            printf("Process %d is deadlocked\n", i+1);
        }
    }
    
    if (flag3 == 0)
        printf("No deadlocks detected\n");
    
    
    
    
    
    
    
}
