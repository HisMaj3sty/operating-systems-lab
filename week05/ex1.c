#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>


sem_t sem;


void* start_routine (void * g)
{
    printf("My number is %d\n", ((int*)g));
    sem_post(&sem);
}



int main()
{
    
    
    sem_init(&sem, 0, 0);
    int n;
    scanf ("%d",&n);
    int i=0;
    while (i<n)
    {
        pthread_t inc_x_thread;
        printf("Now I will create a new thread %d\n", i);
        pthread_create(&inc_x_thread, NULL, start_routine, (void*)i);
        sem_wait(&sem);
        i++;
    }
    
    return 0;
}

//prior to fixing each thread would run at any point in time 
//So threads created later had a chance to output their numbers earlier
//This was an example of race condition between threads in outputting their numbers
