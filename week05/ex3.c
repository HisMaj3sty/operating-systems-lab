 #include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#define MAX_SIZE 1000

int buff[MAX_SIZE];
int delta;
int ran;

int slp[2];

void sleep(int i)
{
    while (slp[i])
    {
    }
    slp[i] = 1;
}

void wakeup(int i)
{
    slp[i] = 0;  
}

void* producer_routine (void * g)
{
    int empt =0;
    ran = 0;
    while (0==0)
    {
        if (delta == MAX_SIZE)
            sleep(0); //sleep?
        
        if (delta== 0)
            empt = 1;
        else 
            empt = 0;
            
        buff[delta] = 1; //eat your ones
        delta = delta + 1;
        
        if (empt)
            wakeup(1);
        
        ran = (ran + 1)%50;
        if (ran == 0)
            printf("I am alive!\n");
    }
}


void* consumer_routine (void * g)
{
    int ful;
    while (0==0)
    {
        if (delta == 0)
            sleep(1); //sleep?
       
        ful = delta == (MAX_SIZE-1);
            
        buff[delta] = 2; //replace ones with twos
        delta = delta - 1;
        
        if (ful)
            wakeup(0);
    }
}


int main()
{
    delta = 0;
    slp[0] = 1;
    slp[1] = 1;
    
    pthread_t produser_thread;
    pthread_t consumer_thread;
    

    pthread_create(&produser_thread, NULL, producer_routine , NULL);
    consumer_routine(NULL);
    
    return 0;
}

