#!/bin/bash

while true; do
    if ln -s file file.lock 2>/dev/null; then 
        D=$(tail -n1 file 2>/dev/null)
        D=$((D + 1))
        echo "$D" >> file
        rm file.lock
    fi
done
    
#Race condition manifested fairly quickly
#The critical region is basically everything inside if statement -- from reading number and incrementing it to writing back
