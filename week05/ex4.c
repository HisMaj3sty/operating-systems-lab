#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>

pthread_t * threads;
char buff[1000];
int numberArray[100000];
int n;
int num;

int gcd(int a, int b)  
{  
    if (a == 0) 
        return b;  
    return gcd(b % a, a);  
}  

int lcm(int a, int b)  
{  
    return (a*b)/gcd(a, b);  
}  




void* start_routine (void * g)
{
    int i = ((int)g);
    int from = i*(num/n);
    int to = (i+1)*(num/n);
    if (i==n-1)
        to=num;
    //printf("I may do from: %d. To: %d\n", from, to); 
    
    int LSD =1;
    for (int fk=from;fk<to;fk++)
        LSD = lcm(LSD, numberArray[fk]);
    
    return LSD;
}


int main(int arg_c, char ** arg_v)
{    
    n = atoi(arg_v[2]);
    
    num = 0;
    
    FILE* f = fopen(arg_v[1], "r");
    while (!(fscanf(f, "%d", &numberArray[num]) == EOF))
    {num+=1;};
    
    threads = malloc(n*sizeof(pthread_t));
    
    for (int i=0;i<n;i++)
        pthread_create((threads+i), NULL, start_routine, (void*)i);
    
    int LSD=1;
    for (int i=0;i<n;i++)
    {
        void * retval;
        pthread_join(threads[i], &retval);
        
        
        LSD=lcm(LSD, (int)retval);
        
    }
        
    printf("Result: %d\n", LSD);     
    return 0;
}
 
