#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>


struct my_files
{
    ino_t inode;
    char names[1000000];
};

struct my_files huh[10000];
int main()
{
    DIR * dirp = opendir("./tmp");
    if (dirp == NULL) 
        return (1); 


    int poi = 0;
    struct dirent * dp;
    while ((dp = readdir(dirp)) != NULL) 
    {
        char buf2[600];
        sprintf(buf2, "./tmp/%s", dp->d_name); 
        struct stat sb;
        if (stat(buf2, &sb) !=0 )
        {
            perror("stat failed");
            return 1;
        }
        int found = 0;
        for (int g=0;g<poi;g++)
        {
            if (huh[g].inode == sb.st_ino)
            {
                strcat(huh[g].names, buf2);
                strcat(huh[g].names, " ");
                found = 1;
                break;
            }
        }
        if (found == 0)
        {
            struct my_files t;
            t.inode = sb.st_ino;
            t.names[0]=0;
            strcat(t.names, buf2);
            strcat(t.names, " ");
            huh[poi] = t;
            poi+=1;
        }
        
    }
    printf("Number of different i-nodes: %d\n", poi);
    for (int g=0;g<poi;g++)
    {
        printf("These are hard-linked: %s\n", huh[g].names);
    }
    closedir(dirp);
    
    
    return 0;
}
