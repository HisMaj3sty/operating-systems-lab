#include<sys/types.h>
#include<sys/stat.h>
#include <fcntl.h>  
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main()
{
    FILE *fp = fopen("/dev/random", "rb");
    if (!fp) 
    {
        perror("fopen");
        return EXIT_FAILURE;
    }
    char buffer[100];
    fread(buffer, sizeof(char), 20, fp);
    
    int j = 0;
    for (int i = 0; i<20;)
    {
        if (isprint(buffer[j]))
        {
            i++;
            printf("%c", buffer[j]);
        }
        j+=1;
    }
    printf("\n");
    return 0;
}
