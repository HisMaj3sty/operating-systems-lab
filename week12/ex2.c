#include<sys/types.h>
#include<sys/stat.h>
#include <fcntl.h>  
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char *argv[] )
{
    
    int fps[argc+1];
    int append;
    if (argc == 1)
        append = 0;
    else
        append = !strcmp(argv[1],  "-a");
    
    for (int i=append+1; i<argc;i++)
    {
        printf(argv[i]);
        if (append)
            fps[i] = open(argv[i], O_WRONLY | O_APPEND | O_CREAT,  S_IRWXU | S_IRWXG | S_IRWXO);
        else
            fps[i] = open(argv[i], O_WRONLY | O_TRUNC | O_CREAT,  S_IRWXU | S_IRWXG | S_IRWXO);
        
        if (fps[i]==-1)
        {
            perror("File could not be opened");
            return 1;
        }
    }
    
    char buf[10];
    int g = 0;
    while ((g = read(0, buf, sizeof(buf)))>0)
    {
        if (g==-1)
        {
            perror("Read failed");
            return 1;
        }
        
        fps[append] = 1;
        for (int i=append; i<argc;i++)
        {
            int p=0;
            while(p < g)
            {
                int h = write(fps[i], buf+p, sizeof(char) * (g - p));
                if (h==-1)
                {
                    perror("Write failed");
                    return 1;
                }
                p = p+h;
            }
        }
    }
    return 0;
}
 
