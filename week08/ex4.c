#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>


int main()
{
    int n=10;
    int * a;
    int i=0;
    while (i<n)
    {
        a = malloc(sizeof(char) * 1024 * 1024* 10); //Assuming char is 1 byte and malloc works instantly 
        if (a == NULL)
        {
            printf("Malloc returned 0, stopping:\n");
            return 0;
        }
        
        memset(a, 0, sizeof(char) * 1024 * 1024* 10);
        struct rusage usage;
        getrusage(RUSAGE_SELF, &usage);
        printf("%ld \n",usage.ru_maxrss);
        i++;
        sleep(1);
    }
    return 0;
}


