#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    int n=10;
    int * a;
    int i=0;
    while (i<n)
    {
        a = malloc(sizeof(char) * 1024 * 1024* 10); //Assuming char is 1 byte and malloc works instantly 
        if (a == NULL)
        {
            printf("Malloc returned 0, stopping:\n");
            return 0;
        }
        
        memset(a, 0, sizeof(char) * 1024 * 1024* 10);
        i++;
        sleep(1);
    }
    return 0;
}


//Since I have lots of memory si and so stay 0 throughout the run
//Also I have lots of programs running -- 10 Mb is not enough to notice

//While running top I can see my program working and consuming 10 mb each second
