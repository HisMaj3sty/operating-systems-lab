#include <stdio.h>
#include <stdlib.h>

int main()
{
    int n;
    int * a;
    
    printf("Specify number of integers:\n");
    scanf("%d", &n);
    a = malloc(sizeof(int) * n);
    if (a == NULL)
    {
        printf("Malloc returned 0, stopping:\n");
        return 0;
    }
    
    for (int i=0;i<n;i++)
    {
        a[i] = i;
        printf("%d ", a[i]);
    }
    printf("\n");
    free(a);
    return 0;
}
