#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void * my_realloc(void * ptr, size_t size, int old_size)
{
    if (ptr == NULL) return malloc(size);
    if (size == 0) return free(ptr), NULL;
    
    void * ptr1 = malloc(size);
    memcpy(ptr1, ptr, (size>old_size) ? old_size : size);
    free(ptr);
    return ptr1;
}


int main()
{
    int n;
    int * a;
    
    printf("Specify number of integers:\n");
    scanf("%d", &n);
    a = malloc(sizeof(int) * n);
    if (a == NULL)
    {
        printf("Malloc returned 0, stopping:\n");
        return 0;
    }
    a = my_realloc(a, sizeof(int) * (n*2), sizeof(int) * n);
    for (int i=0;i<n*2;i++)
    {
        a[i] = i;
        printf("%d ", a[i]);
    }
    printf("\n");
    free(a);
    return 0;
}
