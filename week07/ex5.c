#include <stdio.h> 
int main() 
{ 
    char *h;//add this to intialize **s so that we would not assign unitialized variable
    char **s  = &h;
    char foo[] = "Hello World"; 
    *s = foo;
    printf("*s is %s\n",*s); //add *, because s is not a string
    s[0] = foo;
    printf("s[0] is %s\n",s[0]); 
    return(0); 
} 
