#include <stdlib.h>
#include <stdio.h>



struct page
{
  int number;
  unsigned int referenced;
  int referenced_this_tick;
};

int main()
{
    int n;
    struct page * page_frames;
    printf("Input number of page frames:\n");
    scanf("%d", &n);
    page_frames = malloc(sizeof(struct page)*n);
    for (int i =0; i<n;i++)
    {
        struct page t;
        t.number = -1;
        t.referenced = 0;
        t.referenced_this_tick=0;
        page_frames[i] = t;
    }
    
    printf("Input sequence of page references\n");
    printf("Note that the sequence should end with EOF\n");
    printf("In bash inputing sequence can be ended using ctrl+d combination\n");
    int hit =0;
    int miss =0;
    
    int a;
    
    
    while(scanf("%d", &a)!=EOF)
    {
        int found = -1;
        for (int i=0; i<n;i++)
        {
            if (page_frames[i].number == a)
            {
                found = i;
                break;
            }
        }
        if (found == -1)
        {
            miss++;
            struct page t;
            t.number = a;
            t.referenced = 0;
            t.referenced_this_tick=1;
            int place = 0;
            unsigned int lowest_counter = 4294967295;
            for (int i=0;i<n;i++)
            {
                if (page_frames[i].referenced < lowest_counter)
                {
                    place = i;
                    lowest_counter = page_frames[i].referenced;
                }
            }
            page_frames[place]=t;
        }
        else 
        {
            hit++;
            page_frames[found].referenced_this_tick = 1;
        }
        for (int i=0;i<n;i++)
        {
            page_frames[i].referenced = ((page_frames[i].referenced_this_tick != 0) << 31) | (page_frames[i].referenced >> 1);
        }
        
    }
    printf("Hit: %d\n", hit);
    printf("Miss: %d\n", miss);
    printf("Hit/Miss ratio: %.2f\n", ((float)hit)/miss);
    return 0;
}
