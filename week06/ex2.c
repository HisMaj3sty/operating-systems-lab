#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <unistd.h>

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

struct job
{
    int arrival_time;
    int start_time;
    int waiting_time;
    int completion_time;
    int burst_time;
    int order_number;
    int last_ran;
};




struct job jobs[10000];
struct job running_job;
int number_of_jobs;
int current_time;


struct job initialize_job(int number,int arrival_time, int burst_time)
{
    struct job j;
    j.arrival_time = arrival_time;
    j.burst_time = burst_time;
    j.completion_time = -1;
    j.start_time = -1;
    j.waiting_time = 0;
    j.order_number = number;
    j.last_ran=-1;
    return j;
}

int is_job_complete(struct job * j)
{
    return j->completion_time != -1;
}

int all_jobs_are_complete()
{
    for (int i=0;i<number_of_jobs;i++)
    {
        if (!is_job_complete(&jobs[i]))
            return 0;
    }
    return 1;
}

int is_job_available(struct job * j)
{
    return (j->arrival_time <= current_time) && !is_job_complete(j);
}

int is_any_job_available()
{
    for (int i=0;i<number_of_jobs;i++)
    {
        if (is_job_available(&jobs[i]))
            return 1;
    }
    return 0;
}

int select_closest_job()
{
    int min_arr_time = 999999999;
    int joi = -1;
    for (int i=0;i<number_of_jobs;i++)
    {
        if (is_job_complete(&jobs[i])) 
            continue;
        
        if (min_arr_time == jobs[i].arrival_time)
        {
            if (jobs[i].burst_time < jobs[joi].burst_time)
                joi = i;
        }
        
        if (min_arr_time > jobs[i].arrival_time)
        {
            joi = i;
            min_arr_time = jobs[i].arrival_time;
        }
        
    
    }
    return joi;
}

int schedule_next_job()
{
    if (!is_any_job_available())
        return select_closest_job();
    
    
    int min_bur_time = 999999999;
    int joi = -1;
    for (int i=0;i<number_of_jobs;i++)
    {
        if (!is_job_available(&jobs[i])) 
            continue;
        if (min_bur_time > jobs[i].burst_time)
        {
            joi = i;
            min_bur_time = jobs[i].burst_time;
        }
    }
    return joi;
}

void run_next_job(int number, int runtime)
{
    struct job * j = &jobs[number];
 
    if (current_time < j->arrival_time)
        current_time = j->arrival_time;
    if (runtime == -1)
        runtime = j->burst_time;
    
    if (j->start_time == -1)
    {
        j->start_time = current_time;
        j->waiting_time += -j->arrival_time + j->start_time;
    }
    else
        j->waiting_time += current_time - j->last_ran;

    
    current_time += MIN(j->burst_time, runtime);
    j->burst_time -= runtime;
    if (j->burst_time<=0)
        j->completion_time = current_time;
    
    j->last_ran = current_time;
    
}

void print_string(char * c)
{
    printf("%-10s", c);
}
void print_number(int n)
{
    printf("%-10d", n);
}

void pretty_print_results()
{
    print_string("Jobs");
    print_string("CT");
    print_string("TAT");
    print_string("WT");
    printf("\n");
    int total_tat = 0;
    int total_wat = 0;
    for (int i=0;i<number_of_jobs;i++)
    {
         struct job j = jobs[i];
         print_number(i+1);
         print_number(j.completion_time);
         print_number(j.completion_time - j.arrival_time);
         print_number(j.waiting_time);
         printf("\n");
         
         total_tat += j.completion_time - j.arrival_time;
         total_wat += j.waiting_time;  
    }
    printf("Average Turnaround time: %f\n", ((float)total_tat)/number_of_jobs);
    printf("Average waiting time: %f\n", ((float)total_wat)/number_of_jobs);
}


int main()
{    
    printf("Specify number of jobs:\n");
    scanf("%d", &number_of_jobs);
    
    printf("Specify arrival time and burst time of each job in a format <arrival_time_for_job_i> <burst_time_for_job_i>:\n");
    for (int i=0;i<number_of_jobs;i++)
    {
        int arrt;
        int burt;
        scanf("%d %d", &arrt, &burt);
        jobs[i] = initialize_job(i, arrt, burt);
    }
    current_time=0;
    
    while(!all_jobs_are_complete())
        run_next_job(schedule_next_job(), -1);
    
    
    pretty_print_results();
    
    return 0;
}
 
 
 
