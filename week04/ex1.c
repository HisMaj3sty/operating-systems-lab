#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
    int n=1;
    int pid_t = fork();
    if (pid_t!=0) printf("Hello from parent %d\n", pid_t-n);
    else printf("Hello from child %d\n", pid_t-n);
    return 0;
}
