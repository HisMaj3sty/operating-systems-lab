#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
    for (int i=0;i<5;i++)
        fork();
    sleep(5);    
    
    return 0;
} 

//This was on the lecture:
//Basically, after the fork call forked process also runs remaining forks
//Result is a binary tree of processes
