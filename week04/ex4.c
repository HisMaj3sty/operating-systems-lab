#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

int main()
{
    char buffer[1000];
    printf("Never use 'sudo su' and alike, since it breaks fgets and the terminal will be stuck\n");
    while (strcmp(buffer, "exit\n"))
    {
        printf("$ ");
        fgets(buffer, sizeof(buffer), stdin);
        
        int l = strlen(buffer);
        if (buffer[l-1] == '&')
        {
            buffer[l-1] = '\0';
            if (fork() == 0)
                system(buffer);
        }
        else 
        {
            system(buffer);
        }
    }
    
    return 0;
} 
 
