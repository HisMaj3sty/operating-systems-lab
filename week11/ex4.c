#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>  
#include <unistd.h>

int main()
{
    int fd_1 = open("ex1.txt",  O_RDWR);
    int fd_2 = open("ex1.memcpy.txt",  O_RDWR);
    
    struct stat s;
    fstat(fd_1, &s);
    
    ftruncate(fd_2, s.st_size);
    
    char * str_1 = mmap(NULL,  s.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd_1, 0);
    char * str_2 = mmap(NULL,  s.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd_2, 0);
    
    
    
    memcpy(str_2, str_1,  s.st_size);
    return 0;
} 
