#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>  
#include <unistd.h>

int main()
{
    int fd = open("ex1.txt",  O_RDWR);
    
    //struct stat s;
    //fstat(fd, &s);
    
    ftruncate(fd, 18);
    char * str = mmap(NULL, 18, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    char * s = "This is a nice day";
    
    memcpy(str, s, 18);
    return 0;
}
