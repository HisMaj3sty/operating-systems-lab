#include <stdio.h>
#include <string.h>

int main()
{
    char string[100];
    fgets(string, sizeof(string), stdin);
    for (int i=0;i<strlen(string)/2;i++)
    {
        char c = string[i];
        string[i] = string[strlen(string)-i-1];
        string[strlen(string)-i-1] = c;
    }
    printf("%s\n", string);
    return 0;
}
 
