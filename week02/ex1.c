#include <stdio.h>
#include <limits.h>
#include <float.h>

int main()
{
    int n = INT_MAX;
    float f = FLT_MAX ;
    double d = DBL_MAX;
    
    printf("%zu %d\n", sizeof(n), n);
    printf("%zu %f\n", sizeof(f), f);
    printf("%zu %lf\n", sizeof(d), d);
    return 0;
}
