#include <stdio.h>
#include <string.h>

void xor(int *a, int *b)
{
    *a^=*b; 
}

//https://en.wikipedia.org/wiki/XOR_swap_algorithm
//This algo is usefull here
// (never though I would say this)
int main() 
{
    int n;
    scanf ("%d",&n);
    int m;
    scanf ("%d",&m);
    xor(&n,&m); //This is still technically using a separate function btw
    xor(&m,&n);
    xor(&n,&m);
    printf("%d %d\n",  n, m);
    return 0;
}
 
 
 
