package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AAAAAAAAAAAAAAAAAAAAAAtodolistAAAAAAAAAAAAdapter extends BaseAdapter {
    List<String> property;

    public AAAAAAAAAAAAAAAAAAAAAAtodolistAAAAAAAAAAAAdapter(Context context) {
        property = new ArrayList<>();
        this.context = context;
    }

    Context context;

    public void Add(String s)
    {
        property.add(s);
        this.notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return property.size();
    }

    @Override
    public String getItem(int i) {
        return property.get(i);
    }

    @Override
    public long getItemId(int i) {
        return -1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup container) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.todoitem, container, false);
        }
        convertView.findViewById(R.id.del_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                property.remove(position);
                notifyDataSetChanged();
            }
        });
        ((TextView) convertView.findViewById(R.id.text_b))
                .setText(getItem(position));
        return convertView;
    }
}
