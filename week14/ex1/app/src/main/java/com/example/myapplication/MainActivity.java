package com.example.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity {

    ListView lw;
    AAAAAAAAAAAAAAAAAAAAAAtodolistAAAAAAAAAAAAdapter a;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lw = findViewById(R.id.list_view);
        a = new AAAAAAAAAAAAAAAAAAAAAAtodolistAAAAAAAAAAAAdapter(this);
        lw.setAdapter(a);
        FloatingActionButton fb = findViewById(R.id.f_a_b);
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText taskEditText = new EditText(MainActivity.this);
                AlertDialog dialog = new AlertDialog.Builder(MainActivity.this).setTitle("Add a new task").setMessage("What do you want to do next?").setView(taskEditText).setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override public
                    void onClick(DialogInterface dialog, int which) {
                        String task = String.valueOf(taskEditText.getText());
                        a.Add(task);
                    }
                }).setNegativeButton("Cancel", null).create();
                dialog.show();
            }
        });
    }


}