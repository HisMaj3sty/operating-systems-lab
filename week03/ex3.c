#include <stdio.h>
#include <stdlib.h>

struct element;

struct element
{
    struct element *p;
    int node;
};


void insert_node(struct element ** head, struct element * el)
{
    struct element * h =*head;
    *head = el;
    el->p=h;//insert after head
}

void print_list(struct element ** head)
{
    while (*head!=NULL)
    {
        printf("%d ", (*head)->node);
        head = &(*head)->p;
    }
    printf("\n");
}

void delete_node(struct element ** head, struct element * el)
{
    if (*head == el)
    {
        *head = (*head)->p;
    }
    else
        if (*head != NULL)
        {
            if ((*head)->p == el)
            {
                struct element * fst = *head;
                struct element * sec = (*head)->p->p;
                fst->p=sec;
            }
            else
            {
                delete_node(&(*head)->p, el);
            }
        }
        else 
            return;
}

int main()
{
    
    struct element head = {NULL,2};
    struct element *list = &head;
    struct element el1 = {NULL,4};
    struct element el2 = {NULL,5};
    struct element el3 = {NULL,9};
    struct element el4 = {NULL,7};
    struct element el5 = {NULL,1};
    insert_node(&list, &el1);
    insert_node(&list, &el2);
    insert_node(&list, &el3);
    insert_node(&list, &el4);
    insert_node(&list, &el5);
    print_list(&list);
    delete_node(&list, &el4);
    print_list(&list);

    return 0;
}
