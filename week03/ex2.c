#include <stdio.h>


void bubble_sort(int * a , int n)
{
    int swapped = 0;
   while(!swapped){
       swapped = 1;
        for (int j=0;j<n-1;j++)
        {
            if (a[j]>=a[j+1])
            {
                swapped =0;
                int t = a[j];
                a[j]= a[j+1];
                a[j+1] =t;
            }
        }
   }
}


int main()
{
    
    int n;
    printf("Enter array size:\n");
    scanf("%d", &n);
    int *a;
    a = malloc(sizeof(int)*n);
    printf("Enter array:\n");
    for (int j=0;j<n;j++)
    {
        int t;
        scanf("%d", &t);
        a[j]=t;
    }
    
    
    bubble_sort(a, n);
    for (int j=0;j<n;j++)
    {
        printf("%d ", a[j]);
    }
    printf("\n");
    return 0;
}
